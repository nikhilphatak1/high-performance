#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/sysinfo.h>

#define N 1000 // matrix size

int main() {

    int i, j;
    srand(time(NULL));

    // from: https://stackoverflow.com/questions/4586405/how-to-get-the-number-of-cpus-in-linux-using-c
    printf("This system has %d processors available.\n", get_nprocs());

    int** A_mat = (int**)calloc(sizeof(int*),N);
    for (i = 0; i < N; ++i) {
        A_mat[i] = (int*)calloc(sizeof(int),N);
        for (j = 0; j < N; ++j) {
            A_mat[i][j] = rand()%100;
        }
    }

    int* b_vec = (int*)calloc(sizeof(int),N);
    for (i = 0; i < N; ++i) {
        b_vec[i] = rand()%100;
    }

    int* c_vec = (int*)calloc(sizeof(int),N);
    int tid;
    int c_vec_private[N] = {};
    #pragma omp parallel default(none) private(i,j,tid,c_vec_private) shared(A_mat,b_vec,c_vec)
    {
        tid = omp_get_thread_num();
        printf("Thread %d\n", tid);
        for (i = 0; i < N; i++) {
            #pragma omp parallel reduction(+:c_vec_private[i])
            for (j = 0; j < N; j++) {
                c_vec_private[i] += A_mat[i][j] * b_vec[j];
            }
        }
        #pragma omp for 
        for(i=0; i<N; i++) {
            c_vec[i] += c_vec_private[i];
        }
    
    }
    
    printf("Result:\n");
    for (i = 0; i < N; ++i) {
        printf("%i ",c_vec[i]);
        fflush(stdout);
    }
    printf("\n\n");

    for (i = 0; i < N; ++i) {
        free(A_mat[i]);
    }
    free(A_mat);
    free(b_vec);
    free(c_vec);

    return 0;
}