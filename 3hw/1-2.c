#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define TAYLOR_TERMS 30

/*
f(x) = x - sin(x) in taylor expansion is
f(x) ~= (x^3/3!) - (x^5/5!) + (x^7/7!) - ...
*/

double
factorial(int x) {
    if (x < 0) {
        printf("Negative Factorial\n");
        fflush(stdout);
        exit(1);
    }
    double result = 1;
    for (int ii = x; ii > 0; --ii) {
        result *= ii;
    }
    return result;
}

double
taylor_sign(int xx) {
    //int xx = (int) x;
    //printf("xx = %i\n",xx);
    if (((xx / 2) % 2) == 0) {
        return -1.0;
    }
    return 1.0;

}

double
func(double x) {
    double result = 0;
    for (int ii = 3; ii <= TAYLOR_TERMS; ii = ii + 2) {
        result +=  taylor_sign(ii) * pow(x,ii) / factorial(ii);
        //printf("result = %lf\n",result);
    }
    return result;
}


int
main(int argc, char** argv) {
    if (argc != 2) {
        printf("Wrong number of arguments.\n");
        exit(1);
    }

    double dl;
    sscanf(argv[1],"%lf",&dl); // read input as double
    printf("f(%lf) = %lf",dl,func(dl));

    return 0;

}