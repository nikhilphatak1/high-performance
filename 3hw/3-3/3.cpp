#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

omp_lock_t* forks = NULL;
int nphil;

void simulate(int i)
{
	while (true)
	{
		if (i == 0) {
            omp_set_lock(&forks[nphil-1]);
            printf("p %d took fork %d\n",i,nphil-1);
            omp_set_lock(&forks[i]);
            printf("p %d took fork %d\n", i,i);
            printf("p %d is eating\n", i);
            sleep(1);
            printf("p %d finished eating\n", i);
            omp_unset_lock(&forks[i]);
            omp_unset_lock(&forks[nphil-1]);
        } else {
            omp_set_lock(&forks[i-1]);
            printf("p %d took fork %d\n",i,i-1);
            omp_set_lock(&forks[i]);
            printf("p %d took fork %d\n", i,i);
            printf("p %d is eating\n", i);
            sleep(1);
            printf("p %d finished eating\n", i);
            omp_unset_lock(&forks[i]);
            omp_unset_lock(&forks[i-1]);
        }
	}
}

int  main(int argc, char** argv) {

    if (argc != 2) {
        printf("Incorrect number of arguments.\n");
        return 1;
    }
    
    nphil = atoi(argv[1]);
    printf("Selected %d philosophers.\n",nphil);
    
    forks = (omp_lock_t*) malloc(nphil * sizeof(omp_lock_t));
	omp_set_num_threads(nphil);

	int i;
	for (i = 0; i < nphil; i++)
	{
		omp_init_lock(&forks[i]);
	}	

	#pragma omp parallel for private(i)
	for (i = 0; i < nphil; i++)
	{
		simulate(i);
	}

	return 0;
}