#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>

#define NTDS 4 // number of threads
#define N 1000 // max value to check

typedef struct params {
    int tnum;
} params;

pthread_t* threads = NULL;
sem_t* sem = NULL;
int total_count;

void // credit for check_rv() function to Prof. Nat Tuck from CS3650
check_rv(int rv) {
    if (rv != 0) {
        perror("uh oh\n");
        fflush(stdout);
        fflush(stderr);
        abort();
    }
}

params*
make_params(int tnum) {
    params* par = malloc(sizeof(params));
    par->tnum = tnum;
    return par;
}

void* // entry point for each thread
compute(void* arg) {
    params* par = (params*) arg;
    int tnum = par->tnum;
    int count = 0;
    int i = tnum + 1; // skip zero
    int rv;
    while (i <= N) {

        if ((i % 3 == 0) || (i % 4 == 0)) {
            printf("%i\n",i);
            count++;

        }

        i += NTDS;
    }
    rv = sem_wait(sem);
    check_rv(rv);
    total_count += count;
    rv = sem_post(sem);
    check_rv(rv);
    
}

int main() {

    int i, rv;
    total_count = 0;
    threads = (pthread_t*) malloc(NTDS * sizeof(pthread_t));
    params* pars[NTDS];
    sem = malloc(sizeof(sem_t));
    rv = sem_init(sem,0,1);
    check_rv(rv);
    for (i = 0; i < NTDS; i++) {
        pars[i] = make_params(i);
        rv = pthread_create(&(threads[i]),NULL,&compute,pars[i]);
        check_rv(rv);
    }

    for (i = 0; i < NTDS; i++) {
        rv = pthread_join(threads[i],NULL);
        check_rv(rv);
        free(pars[i]);
    }

    printf("total = %i\n",total_count);

    free(threads);

    return 0;
}