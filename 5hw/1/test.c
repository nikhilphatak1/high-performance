/*
hist-a.c
histogram binning, each node bins 1/#classes^th of the input values
Nikhil Phatak
EECE5640
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>
#define N 100
#define B 2

int main(int argc, char *argv[]) {
    // initialize
    int numprocs,rank,namelen,i,j,retval,gsize,boi,sendcnt=1,root;
    int sendarray[100];
    srand(time(NULL));
    for (i = 0; i < 100; i++) {
        sendarray[i] = rand();
    }
    //MPI_Comm comm;
    int *rbuf;
    // MPI setup
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &gsize);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    //MPI_Get_processor_name(processor_name, &namelen);
    rbuf = (int*)malloc(gsize*100*sizeof(int));
    //rbuf = malloc(numprocs*sizeof(int));
    //histogram = malloc(B*sizeof(int));
    MPI_Gather(sendarray,100,MPI_INT,rbuf,100,MPI_INT,0,MPI_COMM_WORLD);

    /* Concept: 
     * - iterate through bucket values for each node
     * - every iteration, gather indices of bucket values that needs to be iterated
     for (i = rank*(N/numprocs); i < (rank+1)*(N/numprocs); i++) {
        memset(&temp,nums[i]/B,sizeof(int));
        memcpy(&sendbuf,&temp,sizeof(int));
        //MPI_Gather(&sendbuf,sendcnt,MPI_INT,&recvbuf,numprocs,MPI_INT,root,MPI_COMM_WORLD);

        if (rank == 0) {
            for (j = 0; j < numprocs; j++) {
                printf("oh boy %d\n",j);
                //boi = recvbuf+j*sizeof(int);
                memcpy(&histogram,&recvbuf,numprocs*sizeof(int));
                printf("oh fuck\n");
                printf("adding to bucket %d\n",histogram[i]);
                //histogram[*(recvbuf+j)] = histogram[*(recvbuf+j)] + 1;
                printf("small boi\n");
            }
        }
    }
    */
    if (rank == 0) {
        for (i = 0; i < gsize*100; i++)
            printf("%d\n",rbuf[i]);
            
    }
    free(rbuf);
    MPI_Finalize();
    return 0;
}
