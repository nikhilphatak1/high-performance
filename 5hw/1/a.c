/*
hist-a.c
histogram binning, each node bins 1/#classes^th of the input values
Nikhil Phatak
EECE5640
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>
#define MAX 1000       // max value in histogram
#define N 100000000   // number of values in histogram
#define B 2        // number of buckets

int main(int argc, char *argv[]) {

    // initialize
    int numprocs,rank,namelen,i,j,retval,gsize,boi,sendcnt=1,root=0;
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    double hits, x, y, hitsum, pi;
    int *histogram, *sendbuf, *recvbuf;
    sendbuf = malloc(sizeof(int));
    int bucket_size = MAX/B;
    
    // build list of values
    srand(123456);
    int* nums = malloc(N*sizeof(int));
    for (i = 0; i < N; i++) {
        nums[i] = rand() % 1000 + 1;
    }
    
    // MPI setup
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Get_processor_name(processor_name, &namelen);
    
    recvbuf = (int*)malloc(B*sizeof(int));
    histogram = (int*)calloc(B,sizeof(int));
    int tmp;

    /* Concept: 
     * - iterate through bucket values for each node
     * - every iteration, gather indices of bucket values that need to be iterated
     */
    for (i = rank*(N/numprocs); i < (rank+1)*(N/numprocs); i++) {
        tmp = nums[i]/bucket_size;
        //MPI_Gather(sendbuf,1,MPI_INT,recvbuf,1,MPI_INT,root,MPI_COMM_WORLD);
        //tmp = (int)(recvbuf[j]);
        histogram[tmp] = histogram[tmp] + 1;
    }
    
    for (i = 0; i < B; i++) {
        MPI_Reduce(&histogram[i],recvbuf,1,MPI_INT,MPI_SUM,root,MPI_COMM_WORLD);
        if (rank == 0) {
            printf("Bucket %d [%d, %d]: %d\n",i,i*bucket_size+1,(i+1)*bucket_size,*recvbuf);
        }
    }
    /*
    if (rank == 0) {
        for (i = 0; i < B; i++) {
            printf("Bucket %d [%d, %d]: %d\n",i,i*bucket_size+1,(i+1)*bucket_size,recvbuf[i]);
        }
    }
    */

    free(sendbuf);
    free(nums);
    free(histogram);
    MPI_Finalize();
    return 0;
}
