#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define N 100000000 
#define B 8 
#define MAX 1000

__global__ void kernel_getHist(int* array, long size, int* histo, int buckets) {
    
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    if (tid>=size) return;
    int value = array[tid];
    int bucketSize = MAX / buckets;
    int bin = value / bucketSize;
    atomicAdd(&histo[bin],1);
}

void getHist(int* array, long size, int* histo,int buckets) {

    int* dArray;
    cudaMalloc(&dArray,size * sizeof(int));
    cudaMemcpy(dArray,array,size * sizeof(int),cudaMemcpyHostToDevice);

    int* dHist;
    cudaMalloc(&dHist,buckets * sizeof(int));
    cudaMemset(dHist,0,buckets * sizeof(int));
    
    dim3 block(32);
    dim3 grid((size + block.x - 1)/block.x);
    
    //int val;
    //int bucketSize;
    //int bin;

    kernel_getHist<<<grid,block>>>(dArray,size,dHist,buckets);

    /*
    for (int i = 0; i < N; i++) {
        val = array[i];
        bucketSize = MAX / buckets;
        bin = val / bucketSize;
        histo[bin] += 1;
    }
    */

    cudaMemcpy(histo,dHist,buckets * sizeof(int),cudaMemcpyDeviceToHost);
    cudaFree(dArray);
    cudaFree(dHist);
}

int main(int argc, char* argv[]) {

    int* array = (int*)malloc(N*sizeof(int));
    long size = N;
    int* histo = (int*)malloc(B*sizeof(int));
    int buckets = B; 
    
    srand(time(NULL));    

    int i;
    for (i = 0; i < size; i++) {
        array[i] = rand() % 1000 + 1;
        //printf(" %d\n",array[i]);
    }

    getHist(array,size,histo,buckets);

    for (int i = 0; i < B; i++) {
        printf("%d\n",histo[i]);
    }
    
    free(array);
    free(histo);
    return 0;
}
