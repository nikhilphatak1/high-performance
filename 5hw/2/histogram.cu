#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define N 1000
#define B 40
#define MAX 1000
/*
__global__ void (double* a,double* b,double* c,int n) // CUDA kernel
{
    int id = blockIdx.x*blockDim.x+threadIdx.x; // Get global thread ID
    if(id < n) // Make sure we do not go out of bounds
        c[id] = a[id] + b[id];
}
int main(int argc, char* argv[]) {
    int n = 100000;     // Size of vectors
    double *h_a, *h_b;  // Host input vectors
    double *h_c;        //Host output vector
    double *d_a, *d_b;  // Device input vectors
    double *d_c;        // Device output vector
    size_t bytes = n*sizeof(double);    // Size, in bytes, of each vector
    h_a = (double*)malloc(bytes);       // Allocate memory for each vector on host
    h_b = (double*)malloc(bytes);
    h_c = (double*)malloc(bytes);
    cudaMalloc(&d_a, bytes);    //Allocate memory for each vector on GPU
    cudaMalloc(&d_b, bytes);
    cudaMalloc(&d_c, bytes);
    int i;

    for( i = 0; i < n; i++ ) {  // Initialize vector on host
        h_a[i] =sin(i)*sin(i);
        h_b[i] =cos(i)*cos(i);
    }

    cudaMemcpy(d_a, h_a, bytes, cudaMemcpyHostToDevice);//Copy vectors to device
    cudaMemcpy(d_b, h_b, bytes, cudaMemcpyHostToDevice);
    int blockSize, gridSize;
    blockSize = 1024; // Number of threads in each thread block 
    gridSize = (int)ceil((float)n/blockSize);// Number of thread blocks in grid
    // Execute the kernel
    vecAdd<<<gridSize, blockSize>>>(d_a, d_b, d_c, n); // Execute kernel
    cudaMemcpy( h_c, d_c, bytes, cudaMemcpyDeviceToHost ); //Copy back to host
    // Sum up vector c and print result divided by n
    double sum = 0;
    for(i=0; i<n; i++) sum += h_c[i];

    printf("final result: %f\n", sum/n);// The answer should be 1.0
    cudaFree(d_a); // Release device memory
    cudaFree(d_b);
    cudaFree(d_c);
    free(h_a); // Release host memory
    free(h_b);
    free(h_c);
    return 0;
}
*/

__global__ void kernel_getHist(unsigned char* array, long size, unsigned int* histo, int buckets)
{
    int tid = blockIdx.x * blockDim.x + threadIdx.x;

    if(tid>=size)   return;

    unsigned char value = array[tid];

    int bin = value / buckets;

    atomicAdd(&histo[bin],1);
}

void getHist(unsigned char* array, long size, unsigned int* histo,int buckets)
{
    printf("hello2\n");
    unsigned char* dArray;
    cudaMalloc(&dArray,size);
    printf("hello3\n");
    cudaMemcpy(dArray,array,size,cudaMemcpyHostToDevice);

    unsigned int* dHist;
    cudaMalloc(&dHist,buckets * sizeof(int));
    cudaMemset(dHist,0,buckets * sizeof(int));
    printf("hello4\n");

    dim3 block(32);
    dim3 grid((size + block.x - 1)/block.x);
    printf("hello5\n");

    kernel_getHist<<<grid,block>>>(dArray,size,dHist,buckets);
    printf("hello6\n");

    cudaMemcpy(histo,dHist,buckets * sizeof(int),cudaMemcpyDeviceToHost);
    printf("hello7\n");
    //printf("%d\n",dHist[0]);
    cudaFree(dArray);
    cudaFree(dHist);
}

int main(int argc, char* argv[]) {

    unsigned char* array = (unsigned char*)malloc(N*sizeof(char));
    long size = N;
    unsigned int* histo = (unsigned int*)malloc(N*sizeof(int));
    int buckets = B; 
    
    int i;
    for (i = 0; i < size; i++) {
        array[i] = rand() % 256;
    }

    printf("hello1\n");
    getHist(array,size,histo,buckets);
    printf("hello8\n");
    //printf("%p\n",histo);
    
    for (int i = 0; i < B; i++) {
        printf("%d\n",histo[i]);
    }
    
    return 0;

}
