#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <assert.h>
#include <pthread.h>

#include "float_vec.h"
#include "barrier.h"
#include "utils.h"

typedef struct params {
    int pnum;
    floats* input;
    const char* output;
    int P;
    floats* samps;
    long* sizes;
    barrier* bb;
} params;

params*
make_params(int pnum, floats* input, const char* output, int P, floats* samps, long* sizes, barrier* bb) {
    params* par = malloc(sizeof(params));
    par->pnum = pnum;
    par->input = input;
    par->output = output;
    par->P = P;
    par->samps = samps;
    par->sizes = sizes;
    par->bb = bb;
    return par;
}

int
comparer(const void* a,const void* b) {
    float fa = *(const float*) a;
    float fb = *(const float*) b;
    return (fa > fb) - (fa < fb);
}

void
qsort_floats(floats* xs) {
    qsort(xs->data,xs->size,sizeof(float),comparer);
}

floats*
sample(floats* input, int P) {

    floats* samps = make_floats(0);
    floats* samps_fin = make_floats(0);

    for (int pp = 0; pp < 3*(P-1); ++pp) {
        floats_push(samps,input->data[random()%input->size]);
    }

    qsort_floats(samps);

    assert(samps->size == 3*(P-1));

    floats_push(samps_fin,0.0f);

    for (int qq = 1; qq < 3*(P-1); qq = qq + 3) {
        floats_push(samps_fin,samps->data[qq]);
    }

    floats_push(samps_fin,INFINITY);

    assert(samps_fin->size == P+1);

    free_floats(samps);

    return samps_fin;
}

void*
sort_worker(void* parm) {

    params* par = (params*) parm;
    floats* xs = make_floats(0);

    /* get the values in this process' sampling range */
    for (int ii = 0; ii < par->input->size; ++ii) {//xs->size; ++ii) {
        if (par->samps->data[par->pnum+1] > par->input->data[ii] && par->input->data[ii] >= par->samps->data[par->pnum]) {
            floats_push(xs,par->input->data[ii]);
        }
    }

    printf("%d: start %.04f, count %ld\n", par->pnum, par->samps->data[par->pnum], xs->size);

    par->sizes[par->pnum] = xs->size;

    qsort_floats(xs);

    barrier_wait(par->bb);

    /* compute the address at which to start writing */
    int start = 0;
    for (int jj = 0; jj < par->pnum; ++jj) {
        start += par->sizes[jj];
    }

    /* seek the starting position */
    int ofd = open(par->output, O_WRONLY, 0644);
    check_rv(ofd);
    int rv = lseek(ofd,sizeof(long)+sizeof(float)*start,SEEK_SET);
    check_rv(rv);

    /* write local array to file */
    write(ofd,&(xs->data[0]),xs->size*sizeof(float));

    /* clean up */
    rv = fsync(ofd);
    check_rv(rv);
    rv = close(ofd);
    check_rv(rv);
    free_floats(xs);

    return 0;
}

void
run_sort_workers(floats* input, const char* output, int P, floats* samps, long* sizes, barrier* bb)
{
    pthread_t threads[P];
    params* pars[P];

    for (int ii = 0; ii < P; ++ii) {
        pars[ii]  = make_params(ii, input, output, P, samps, sizes, bb);
        int rv = pthread_create(&(threads[ii]), 0, sort_worker, pars[ii]);
        check_rv(rv);
    }

    for (int jj = 0; jj < P; ++jj) {
        int rv = pthread_join(threads[jj], 0);
        check_rv(rv);
        free(pars[jj]);
    }
}

void
sample_sort(floats* input, const char* output, int P, long* sizes, barrier* bb)
{
    floats* samps = sample(input, P);
    run_sort_workers(input, output, P, samps, sizes, bb);
    free_floats(samps);
}

int
main(int argc, char* argv[])
{
    alarm(120);
    int rv = 0;

    if (argc != 4) {
        printf("Usage:\n");
        printf("\t%s P input.dat output.dat\n", argv[0]);
        return 1;
    }

    const int P = atoi(argv[1]);
    const char* iname = argv[2];
    const char* oname = argv[3];

    seed_rng();

    long arrsize;
    barrier* bb = make_barrier(P);
    long* sizes = malloc(P * sizeof(long));

    /* OPEN I/O FILES */
    int ifd = open(iname, O_RDONLY);
    check_rv(ifd);
    int ofd = open(oname, O_CREAT|O_TRUNC|O_WRONLY, 0644);
    check_rv(ofd);

    /* READ ARRAY SIZE */
    rv = read(ifd,&arrsize,sizeof(arrsize));
    check_rv(rv);
    floats* input = make_floats(arrsize);

    /* READ FLOATS INTO STRUCT */
    rv = read(ifd,&(input->data[0]),arrsize*sizeof(float));
    check_rv(rv);

    /* truncate the file */
    int if_size = (arrsize * sizeof(float)) + sizeof(long);
    rv = ftruncate(ofd,(off_t)if_size);
    check_rv(rv);

    /* write size to output file */
    rv = write(ofd,&arrsize,sizeof(arrsize));
    check_rv(rv);

    /* file descriptor clean up */
    rv = fsync(ofd);
    check_rv(rv);
    rv = close(ifd);
    check_rv(rv);
    rv = close(ofd);
    check_rv(rv);

    /* call sort */
    sample_sort(input, oname, P, sizes, bb);

    /* memory clean up */
    free(sizes);
    free_barrier(bb);
    free_floats(input);

    return 0;
}
