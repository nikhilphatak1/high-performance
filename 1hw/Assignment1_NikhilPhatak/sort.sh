#!/bin/bash

make
./tools/gen-input 1000000 input.dat
time -p ./tssort 8 input.dat output.dat
./tools/check-sorted output.dat
