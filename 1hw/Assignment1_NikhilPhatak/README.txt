EECE5640 Assignment 1

To compile this project, run: 

make

To run the compiled executable, run:

./tssort P input.dat output.dat

Where input.dat is the datafile of integers generated with the following command:

./tools/gen-input 1000000 input.dat

and "P" is the number of threads to create and use for sorting.