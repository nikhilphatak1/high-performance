/*
mpipi.c
compute pi using the dartboard method and mpi
Nikhil Phatak
EECE5640
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <mpi.h>
#define R 1.0
#define SAMPLE 1E7

int in_circle(float x, float y) {
    return (x*x) + (y*y) <= R*R;
    //return x*x + y*y <= 1;
}

double rand2() {
    return (double)rand()/(double)RAND_MAX;
}

int main(int argc, char *argv[]) {

    int numprocs, rank, namelen, i, retval;
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    double hits, x, y, hitsum, pi;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Get_processor_name(processor_name, &namelen);
   
    srand(time(NULL)*rank);
    hits = 0;
    for (i = 0; i < SAMPLE/numprocs; i++) {
        x = (-1.0*R) + fmod(rand2()*(double)rand(),2.0*R);
        y = (-1.0*R) + fmod(rand2()*(double)rand(),2.0*R);
        //x = -0.5 + rand2();
        //y = -0.5 + rand2();
        hits += in_circle(x,y) ? 1 : 0;
        //if (i % (int)(SAMPLE/10) == 0) {
        retval = MPI_Reduce(&hits, &hitsum, 1, MPI_DOUBLE, MPI_SUM,0, MPI_COMM_WORLD);
        pi = 4.0 * hitsum/(float)((i+1)*numprocs);
        if (rank == 0) {
            if (i % (int)(SAMPLE/(10*numprocs)) == 0)
                printf("after %i throws, pi = %10.8f\n",(i+1)*numprocs,pi);
        }
        
    }

    if (rank == 0)
        printf("pi estimate = %10.8f\n",pi);
    
    MPI_Finalize();
    return 0;
}
