Nikhil Phatak
Professor David Kaeli
TA Trinayan Baruah

Compilation and run command:

`mpicc mpipi.c -lm && mpirun a.out`


To compile, run, and time the run:

`mpicc mpipi.c -lm && time -p mpirun a.out`

