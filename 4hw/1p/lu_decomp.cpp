#include <iostream>
#include <cstdio>
#define N 3
//using namespace std;

void lu(float a[][10], float l[][10], float u[][10], int n){
    int i = 0, j = 0, k = 0;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if (j < i) {
                l[j][i] = 0;
            } else {
                l[j][i] = a[j][i];
                for (k = 0; k < i; k++) {
                    l[j][i] = l[j][i] - l[j][k] * u[k][i];
                }
            }
        }

        for (j = 0; j < n; j++) {
            if (j < i) {
                u[i][j] = 0;
            }
            else if (j == i) {
                u[i][j] = 1;
            } else {
                u[i][j] = a[i][j] / l[i][i];
                for (k = 0; k < i; k++) {
                    u[i][j] = u[i][j] - ((l[i][k] * u[k][j]) / l[i][i]);
                }
            }
        }
    }
}

void output(float x[][10], int n) {
    int i = 0, j = 0;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            printf("%f ", x[i][j]);
        }
        std::cout << "\n";
    }
}

double CLOCK() {
        struct timespec t;
        clock_gettime(CLOCK_MONOTONIC,  &t);
        return (t.tv_sec * 1000)+(t.tv_nsec*1e-6);
}

int main(int argc, char **argv) {

    //void lu(float[][10], float[][10], float[][10], int n);
    //void output(float[][10], int);
    float a[10][10], l[10][10], u[10][10];
    int n = 0, i = 0, j = 0;
    std::cout << "Enter size of 2d array(Square matrix) : ";
    std::cin >> n;

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            std::cout << "Enter values no:" << i << ", " << j << ": ";
            std::cin >> a[i][j];
        }
    }

    lu(a, l, u, n);
    std::cout << "\nL Decomposition\n\n";
    output(l, n);
    std::cout << "\nU Decomposition\n\n";
    output(u, n);

    invert_l()

    return 0;
}