#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#define SIZE 10//24

/*
typedef struct sqmat {
    int size;
    int** mat;
} sqmat;

sqmat* create_sqmat(int s) {
    int i;
    sqmat* m = malloc(sizeof(sqmat));
    m->size = s;
    m->mat = malloc(sizeof(int**));
    m->mat = calloc(size, sizeof(int*));
    for (i = 0; i < size; i++) {
        m->mat[i] = malloc(sizeof(int*));
    }
}
*/
#include <cstdio>

double CLOCK() {
        struct timespec t;
        clock_gettime(CLOCK_MONOTONIC,  &t);
        return (t.tv_sec * 1000)+(t.tv_nsec*1e-6);
}

extern "C" {
    // LU decomoposition of a general matrix
    void dgetrf_(int* M, int *N, double* A, int* lda, int* IPIV, int* INFO);

    // generate inverse of a matrix given its LU decomposition
    void dgetri_(int* N, double* A, int* lda, int* IPIV, double* WORK, int* lwork, int* INFO);
}

void inverse(double* A, int N)
{
    int *IPIV = new int[N+1];
    int LWORK = N*N;
    double *WORK = new double[LWORK];
    int INFO;

    dgetrf_(&N,&N,A,&N,IPIV,&INFO);
    dgetri_(&N,A,&N,IPIV,WORK,&LWORK,&INFO);

    delete IPIV;
    delete WORK;
}

int main(){

    double A [2*2] = {
        1,2,
        3,4
    };

    inverse(A, 2);

    printf("%f %f\n", A[0], A[1]);
    printf("%f %f\n", A[2], A[3]);

    return 0;
}