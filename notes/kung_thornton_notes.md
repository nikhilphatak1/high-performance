# Why Systolic Architectures?

- hpc systems are usually for specific requirements
- repeated errors
- io/computation imbalance
- io interfaces can't keep up with device speed
- systolic architecture: general methodology for mapping high level computations to hardware

## Systolic Architecture
- data flows from memory in a rhythmic fashion
- passes through many computing elements before returning to memory
- (like blood through the heart)
- two dimensional (triangle, hexagon, rectangle) instead of linear assembly line
    - higher parallelism
- data flow at multiple speeds/directions
- inputs and partial results both flow
- only results flow in classical systems
- easy to implement/reconfigure due to modularity

## Designing special purpose systems
- task definition, design, implelmentation
- task definition
    - bottleneck identified
    - decide whether to resolve with special purpose hardware
- costs can be classified as:
    - nonrecurring (design)
    - recurring (parts)
- design cost of special purpose system must be relatively small

## Concurrency and Communication
- to make system fast: fast components or concurrency
- need concurrent algorithms
- design needs to be modular to match io speed

- small special system, large computation:
    - requires decomposing computation and storing intermediates
    - number of io ops is O(n log n / log S)

## Design of systolic system
- cells in system usually interconnected in a systolic array or tree
- external communication occurs only at boundary cells
- cells on boundaries are usually io cells
- classify computation tasks
    - io bound (adding 2 matrices)
    - compute bound (multiplying 2 matrices)

## Basic principle
- replace single processing element with an array of PEs
- higher computation throughput without increasing memory bandwidth
- once something taken from memory, it can be used at each PE and pumped along array
- MISD optimized
- other advantages
    - modular expansibility
    - simple and regular data and control flows
    - eliminate global broadcasting
- convolution problem is compute-bound

- adder can be implemented as a pipelined adder tree
- can have systolic system without global communication
- arbitrary number of cells
- half the cells work at any time unless convolution computations are interleaved
- if there are more weights than cells, static partial results use less io than moving partial results

## Evaluating Systolic Systems
- design makes multiple use of each input data item
- design uses extensive concurrency
- there are only a few types of simple cells

- when memory speed is more than cell speed, 2d systolic arrays should be used
    - every boundary cell transfers data at every cycle so bandwidth of fast memory is fully used
- large 2d arrays might need a slower clock to compensate for clock skews
- address indexing overhead eliminated
- von Neumann bottleneck: bottleneck due to limited memory bandwidth
- 