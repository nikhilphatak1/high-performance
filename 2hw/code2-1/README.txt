Nikhil Phatak

Philosophers Problem
To build and run:

`gcc -o simulate philosophers.c -lpthread && ./simulate n`

where n is the number of philosophers and forks 
to simulate.