Nikhil Phatak

Prime Numbers Problem
To build and run:

`gcc -o sieve sieve.c -lpthread -lm && ./sieve nthreads N`

where nthreads is the number of threads to use
and N is the largest number.