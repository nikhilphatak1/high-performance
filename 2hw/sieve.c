/*
sieve.c
Nikhil Phatak
Professor David Kaeli
TA Trinayan Baruah
*/

#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

pthread_t* threads = NULL;
int* primes = NULL;
int maxval;
int N;

typedef struct params {
    int tnum;
    int begin;
    int end;
} params;

void // credit for check_rv() function to Prof. Nat Tuck from CS3650
check_rv(int rv) {
    if (rv == -1) {
        perror("uh oh\n");
        fflush(stdout);
        fflush(stderr);
        abort();
    }
}

params*
make_params(int tnum, int begin, int end) {
    params* par = malloc(sizeof(params));
    par->tnum = tnum;
    par->begin = begin;
    par->end = end;
    return par;
}

void
print_primes(int zeros) {
    if (!primes) {
        printf("Array primes has not been initialized.\n");
        return;
    }
    if (zeros) {
        for (int i = 1; i <= N; i++) {
            printf("%d\n",primes[i-1]);
        }
    } else {
        for (int i = 1; i <= N; i++) {
            if (primes[i-1]) {
                printf("%d\n",primes[i-1]);
            }
        }
    }
}

void*
sieve(void* arg) {
    params* par = (params*) arg;
    int tnum = par->tnum;
    int begin = par->begin;
    int end = par->end;
    int i, j;

    if (begin % 2 == 0) {
        begin++;
    }

    for (i = begin; i < end; i += 2) {
        j = 3;
        while (j <= (int)sqrt(i)) {
            if (i % j == 0) {
                primes[i-1] = 0;
                break;
            }
            j+=2;
        }
    }
}

int
main(int argc, char** argv) {

    if (argc != 3) {
        printf("Incorrect number of arguments.\n");
        return 1;
    }

    int i, rv, ntds, nptd;
    

    ntds = atoi(argv[1]);
    N = atoi(argv[2]);
    printf("Selected %d threads and max value %d.\n",ntds,N);

    primes = (int*) calloc((size_t) (N+1),sizeof(int));
    primes[1] = 2;
    primes[2] = 3;

    for (i = 5; i <= N; i += 2) { // skip evens
        if (i%3!=0) { // skip mults of 3
            primes[i-1] = i;
        }
    }

    threads = (pthread_t*) malloc(ntds * sizeof(pthread_t));
    params* pars[ntds];
    nptd = N / ntds;
    for (i = 0; i < ntds; i++) {
        if (i == ntds - 1) {
            pars[i] = make_params(i,i*nptd,N+1);
        } else {
            pars[i] = make_params(i,i*nptd,(i+1)*nptd);
        }
        rv = pthread_create(&(threads[i]),NULL,&sieve,pars[i]);
        check_rv(rv);
    }

    for (i = 0; i < ntds; i++) {
        rv = pthread_join(threads[i],NULL);
        check_rv(rv);
        free(pars[i]);
    }

    print_primes(0);

    return 0;
}