/*
phil.c
Nikhil Phatak
Professor David Kaeli
TA Trinayan Baruah
*/

/*
Invariants/Rules:
- fork n is on the right of philosopher n
- philosopher status 0 means thinking, 1 means eating
- fork status 0 means not available, 1 means available
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

sem_t* forks = NULL;
pthread_t* threads = NULL;
int* status = NULL;
int* fork_status;
int nphil;
int* status_copy = NULL;
int* fork_status_copy = NULL;

typedef struct params {
    int tnum;
} params;

void // credit for check_rv() function to Prof. Nat Tuck from CS3650
check_rv(int rv) {
    if (rv == -1) {
        perror("uh oh\n");
        fflush(stdout);
        fflush(stderr);
        abort();
    }
}

params*
make_params(int tnum) {
    params* par = malloc(sizeof(params));
    par->tnum = tnum;
    return par;
}

void
write_status(int tnum, int eating) {
    status[tnum] = eating;
    //printf("Phil %d status now %d\n",tnum,eating);
}

void*
print_status(void* arg) {
    while (1) {
        //int* status_copy = malloc(nphil * sizeof(int));
        //int* fork_status_copy = malloc(nphil * sizeof(int));
        memcpy(status_copy,status,nphil*sizeof(int));
        memcpy(fork_status_copy,fork_status,nphil*sizeof(int));
        for (int i = 0; i < nphil; i++) {
            printf("Phil %d status %d\nFork %d status %d\n",i,status_copy[i],i,fork_status_copy[i]);
        }
        printf("\n");
        sleep(1);
    }
}

void
update_left(int tnum, int fork) {
    if (tnum == 0) { // edge case for left fork
        if (fork == 0) {
            sem_wait(&forks[nphil-1]); // taking last fork
            printf("Phil %d claims fork %d\n",tnum,nphil-1);
            fork_status[nphil-1] = 0;
        } else {
            sem_post(&forks[nphil-1]); // replacing last fork
            printf("Phil %d replaces fork %d\n",tnum,nphil-1);
            fork_status[nphil-1] = 1;
        }
    } else {
        if (fork == 1) {
            sem_wait(&forks[tnum-1]); // taking left fork
            printf("Phil %d claims fork %d\n",tnum,tnum-1);
            fork_status[tnum-1] = 0;
        } else {
            sem_post(&forks[tnum-1]); // replacing left fork
            printf("Phil %d claims fork %d\n",tnum,tnum-1);
            fork_status[tnum-1] = 1;
        }
    }
}

void
update_right(int tnum, int fork) {
    if (tnum == nphil - 1) { // edge case for right fork
        if (fork == 0) {
            sem_wait(&forks[0]); // taking first fork
            printf("Phil %d claims fork %d\n",tnum,0);
            fork_status[0] = 0;
        } else {
            sem_post(&forks[0]); // replacing first fork
            printf("Phil %d replaces fork %d\n",tnum,0);
            fork_status[0] = 1;
        }
    } else {
        if (fork == 1) {
            sem_wait(&forks[tnum]); // taking left fork
            printf("Phil %d claims fork %d\n",tnum,tnum);
            fork_status[tnum] = 0;
        } else {
            sem_post(&forks[tnum]); // replacing left fork
            printf("Phil %d replaces fork %d\n",tnum,tnum);
            fork_status[tnum] = 1;
        }
    }
}

void* // entry point for each thread
simulate(void* arg) {
    params* par = (params*) arg;
    int tnum = par->tnum;
    //printf("Phil %d init\n", tnum);
    write_status(tnum,0);
    while (1) {
        update_left(tnum,0); // get one fork, thinking
        //sleep(1);
        update_right(tnum,0);
        write_status(tnum,1); // get second fork, eating
        printf("Phil %d eating\n",tnum);
        sleep(3);
        update_left(tnum,1); // replace first fork, thinking
        update_right(tnum,1); // replace second fork, thinking
        write_status(tnum,0); // thinking
        printf("Phil %d thinking\n",tnum);
        //sleep(1);
    }
}

int
main(int argc, char** argv) {

    if (argc != 2) {
        printf("Incorrect number of arguments.\n");
        return 1;
    }

    int i, rv;
    

    nphil = atoi(argv[1]);
    printf("Selected %d philosophers.\n",nphil);
    
    forks = (sem_t*) malloc(nphil * sizeof(sem_t));
    threads = (pthread_t*) malloc(nphil * sizeof(pthread_t));
    status = (int*) malloc(nphil * sizeof(int));
    params* pars[nphil];
    fork_status = (int*) malloc(nphil * sizeof(int));
    status_copy = malloc(nphil * sizeof(int));
    fork_status_copy = malloc(nphil * sizeof(int));

    pthread_t* status_thread = (pthread_t*) malloc(sizeof(pthread_t));
    
    for (i = 0; i < nphil; i++) {
        sem_init(&forks[i],0,0);
    }

    pthread_create(status_thread,NULL,&print_status,NULL);

    for (i = 0; i < nphil; i++) {
        pars[i] = make_params(i);
        rv = pthread_create(&(threads[i]),NULL,&simulate,pars[i]);
        check_rv(rv);
    }

    for (i = 0; i < nphil; i++) {
        rv = pthread_join(threads[i],NULL);
        check_rv(rv);
        free(pars[i]);
    }
    rv = pthread_join(*status_thread,NULL);
    check_rv(rv);

    free(forks);
    free(threads);
    free(status_thread);
    free(status);
    free(fork_status);
    free(status_copy);
    free(fork_status_copy);

    return 0;
}