/*
phil.c
Nikhil Phatak
Professor David Kaeli
TA Trinayan Baruah
*/

/*
Invariants/Rules:
- fork n is on the right of philosopher n
- philosopher status 0 means thinking, 1 means eating
- fork status 0 means not available, 1 means available
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

pthread_mutex_t* forks = NULL;
pthread_t* threads = NULL;
int* status = NULL;
int* fork_status;
int nphil;
int* status_copy = NULL;
int* fork_status_copy = NULL;

typedef struct params {
    int tnum;
} params;

void // credit for check_rv() function to Prof. Nat Tuck from CS3650
check_rv(int rv) {
    if (rv == -1) {
        perror("uh oh\n");
        fflush(stdout);
        fflush(stderr);
        abort();
    }
}

params*
make_params(int tnum) {
    params* par = malloc(sizeof(params));
    par->tnum = tnum;
    return par;
}

void*
print_status(void* arg) {
    while (1) {
        int i;
        memcpy(status_copy,status,nphil*sizeof(int));
        memcpy(fork_status_copy,fork_status,nphil*sizeof(int));
        for (i = 0; i < nphil; i++) {
            printf("Phil %d status %d\nFork %d status %d\n",i,status_copy[i],i,fork_status_copy[i]);
        }
        printf("\n");
        sleep(1);
    }
}

void* // entry point for each thread
simulate(void* arg) {
    params* par = (params*) arg;
    int tnum = par->tnum;

    while (1) {
        pthread_mutex_lock(&forks[tnum]);
        fork_status[tnum] = 0;
        pthread_mutex_lock(&forks[(tnum+1)%nphil]);
        fork_status[(tnum+1)%nphil] = 0;
        status[tnum] = 1;
        sleep(3);
        pthread_mutex_unlock(&forks[tnum]);
        fork_status[tnum] = 1;
        pthread_mutex_unlock(&forks[(tnum+1)%nphil]);
        fork_status[(tnum+1)%nphil] = 1;
        status[tnum] = 0;
    }
}

int
main(int argc, char** argv) {

    if (argc != 2) {
        printf("Incorrect number of arguments.\n");
        return 1;
    }

    int i, rv;
    

    nphil = atoi(argv[1]);
    printf("Selected %d philosophers.\n",nphil);
    
    forks = (pthread_mutex_t*) malloc(nphil * sizeof(pthread_mutex_t));
    threads = (pthread_t*) malloc(nphil * sizeof(pthread_t));
    status = (int*) malloc(nphil * sizeof(int));
    params* pars[nphil];
    fork_status = (int*) malloc(nphil * sizeof(int));
    status_copy = malloc(nphil * sizeof(int));
    fork_status_copy = malloc(nphil * sizeof(int));

    pthread_t* status_thread = (pthread_t*) malloc(sizeof(pthread_t));
    
    for (i = 0; i < nphil; i++) {
        pthread_mutex_init(&forks[i],0);
    }

    pthread_create(status_thread,NULL,&print_status,NULL);

    sleep(1);

    for (i = 0; i < nphil; i++) {
        pars[i] = make_params(i);
        rv = pthread_create(&(threads[i]),NULL,&simulate,pars[i]);
        check_rv(rv);
    }

    for (i = 0; i < nphil; i++) {
        rv = pthread_join(threads[i],NULL);
        check_rv(rv);
        free(pars[i]);
    }
    rv = pthread_join(*status_thread,NULL);
    check_rv(rv);

    free(forks);
    free(threads);
    free(status_thread);
    free(status);
    free(fork_status);
    free(status_copy);
    free(fork_status_copy);

    return 0;
}