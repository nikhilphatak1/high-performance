#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define N 64
#define MAX 1E4
/*
float a[N][N][N], b[N][N][N];
for (i = 1; i < n-1; i++) {
    for (j = 1; j < n-1; j++) {
        for (k = 1; k < n-1; k++) {
            a[i][j][k] = 0.8*(b[i-1][j][k] + b[i+1][j][k] + b[i][j-1][k] + b[i][j+1][k] + b[i][j][k-1] + b[i][j][k+1]);
}
*/

__device__ int indexing(int i, int j, int k, int size) {
    return i + j*size + k*size*size;
}

__global__ void kernel_stencil(float *A, float *B, int size) {
    
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int k = blockIdx.z * blockDim.z + threadIdx.z;

    if (i*j*k>=size*size*size || i==0 || j==0 || k==0 || i==N-1 || j==N-1 || k==N-1) return;
    
    A[indexing(i,j,k,size)] = 0.8*(B[indexing(i-1,j,k,size)]+B[indexing(i+1,j,k,size)]+B[indexing(i,j-1,k,size)]+B[indexing(i,j+1,k,size)]+B[indexing(i,j,k-1,size)]+B[indexing(i,j,k+1,size)]);
}

double CLOCK() {
        struct timespec t;
        clock_gettime(CLOCK_MONOTONIC,  &t);
        return (t.tv_sec * 1000)+(t.tv_nsec*1e-6);
}

void stencil(float A[N][N][N], float B[N][N][N]) {

    float *k,*kA,*kB;
    double start, elapsed;
    start = CLOCK();
    cudaMalloc(&k,sizeof(float));
    cudaFree(k);
    elapsed = CLOCK() - start;
    //printf("warm-up malloc & free: %fms\n",elapsed);

    start = CLOCK();
    cudaMalloc(&kB,N*N*N*sizeof(float));
    elapsed = CLOCK() - start;
    //printf("cudaMalloc kB: %fms\n",elapsed);

    start = CLOCK();
    cudaMemcpy(kB,B,N*N*N*sizeof(float),cudaMemcpyHostToDevice);
    elapsed = CLOCK() - start;
    //printf("cudaMemcpy B --> kB: %fms\n",elapsed);

    start = CLOCK();
    cudaMalloc(&kA,N*N*N*sizeof(int));
    elapsed = CLOCK() - start;
    //printf("cudaMalloc kA: %fms\n",elapsed);

    start = CLOCK();
    cudaMemset(kA,0,N*N*N*sizeof(int));
    elapsed = CLOCK() - start;
    //printf("cudaMemset: %fms\n",elapsed);

    dim3 threads_per_block(1,1,1);
    dim3 num_blocks(N/threads_per_block.x,N/threads_per_block.y,N/threads_per_block.z);
    
    start = CLOCK();
    kernel_stencil<<<num_blocks,threads_per_block>>>(kA,kB,N);
    elapsed = CLOCK() - start;
    printf("kernel: %fms\n",elapsed);

    start = CLOCK();
    cudaMemcpy(A,kA,N*N*N*sizeof(float),cudaMemcpyDeviceToHost);
    elapsed = CLOCK() - start;
    //printf("cudaMemcpy kA --> A: %fms\n",elapsed);

    cudaFree(kA);
    cudaFree(kB);
}

int main(int argc, char* argv[]) {

    float A[N][N][N], B[N][N][N]; 
    
    srand(time(NULL));    
    int i, j, k, i_rand = (rand() % (N-2)) + 1, j_rand = (rand() % (N-2)) + 1, k_rand = (rand() % (N-2)) + 1;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            for (k = 0; k < N; k++) {
                B[i][j][k] = (float)i*(float)j*(float)k/((float)i+(float)j+(float)k);///fmod(rand(),MAX); //(((float)i * (float)k) / (float)j);
            }
        }
    }
    
    double start = CLOCK();
    stencil(A,B);
    double elapsed = CLOCK() - start;

    //printf("Coordinates: (%d,%d,%d)\n%6f %6f %6f\t%6f %6f %6f\n%6f %6f %6f\t%6f %6f %6f\n%6f %6f %6f\t%6f %6f %6f\n",i_rand,j_rand,k_rand,B[i_rand-1][j_rand-1][k_rand],B[i_rand][j_rand-1][k_rand],B[i_rand+1][j_rand-1][k_rand],A[i_rand-1][j_rand-1][k_rand],A[i_rand][j_rand-1][k_rand],A[i_rand+1][j_rand-1][k_rand],B[i_rand-1][j_rand][k_rand],B[i_rand][j_rand][k_rand],B[i_rand+1][j_rand][k_rand],A[i_rand-1][j_rand][k_rand],A[i_rand][j_rand][k_rand],A[i_rand+1][j_rand][k_rand],B[i_rand-1][j_rand+1][k_rand],B[i_rand][j_rand+1][k_rand],B[i_rand+1][j_rand+1][k_rand],A[i_rand-1][j_rand+1][k_rand],A[i_rand][j_rand+1][k_rand],A[i_rand+1][j_rand+1][k_rand]);
    
    //printf("Elapsed time = %fms\n",elapsed);
    
    return 0;
}
